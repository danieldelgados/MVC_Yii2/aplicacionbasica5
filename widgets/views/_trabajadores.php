<?php
use yii\helpers\Html;
foreach($modelos as $trabajador){
?>

<div class="panel panel-success">
        <div class="panel-heading"><h2><?= $trabajador->id; ?> </h2></div>
        <div class="panel-body">
            <p><h3>Nombre: <?= $trabajador->nombre; ?> </h3></p>
            <p><h3>Apellidos: <?= $trabajador->apellidos; ?> </h3></p>
            <div class="row row-flex-wrap">
                <div class="col-xs-6 col-md-3 pull-right flex-col">
                    <figure>
                       <?= Html::img('@web/imgs/'.$trabajador->foto, ['alt' => 'fotoTrabajador','class'=>'img-responsive img-circle'])?>
                    </figure>
                </div>
                
            </div>
        </div>
 </div>

<?php
}
?>