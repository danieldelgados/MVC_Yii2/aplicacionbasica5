<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Trabajadores;
use app\models\Delegacion;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionConsulta12(){
        $datos= Trabajadores::find()->all();
                
        return $this->render('consulta12',[
            "trabajadores"=>$datos,
            
        ]);
    }
    public function actionConsulta14(){
        /**
         * activeRecord
         */
        $datos=Delegacion::find()->all();
        /**
         * createCommand
         */
        $datos1=Yii::$app
                ->db
                ->createCommand("select * from delegacion")
                ->queryAll();
        
        /**
         * query
         */
        $query=new \yii\db\Query();
        $datos2=$query
                ->select('*')
                ->from('delegacion')
                ->all();
        
        return $this->render('consulta14',[
            'delegacionesActive'=>$datos,
            'delegacionesCommand'=>$datos1,
            'delegacionesQuery'=>$datos2,
        ]);
    }
    public function actionConsulta17(){
        
        /**
         * ActiveRecord
         */
        $lista= Delegacion::find()
                ->where([
                    'poblacion'=>'Santander'
                    ])
                ->all();
        $lista1= Trabajadores::find()
                ->where([
                    'delegacion'=>1
                    ])
                ->all();
        $lista2= Trabajadores::find()
                ->orderBy([
                    "nombre"=>SORT_ASC
                    ])
                ->all();
        $lista3= Trabajadores::find()
                ->where([
                    'fechaNacimiento'=>'null'
                    ])
                ->all();
        /**
         * createCommand
         */
        $datos=Yii::$app
                ->db
                ->createCommand("select * from delegacion where poblacion='Santander'")
                ->queryAll();
        $datos1=Yii::$app
                ->db
                ->createCommand("select * from trabajadores where delegacion=1")
                ->queryAll();
        $datos2=Yii::$app
                ->db
                ->createCommand("select * from trabajadores order by nombre")
                ->queryAll();
        $datos3=Yii::$app
                ->db
                ->createCommand("select * from trabajadores where fechaNacimiento=0")
                ->queryAll();
        
        return $this->render('consulta17',[
            
            "resultados"=>[$lista,$lista1,$lista2,$lista3,$datos,$datos1,$datos2,$datos3,]
            
            
        ]);
    }
    
    public function actionConsulta20(){
        $lista= Delegacion::find()
                ->andWhere([
                  "<>","poblacion","Santander"  
                ])
                ->andWhere([
                    "<>","direccion","null"
                ])
                ->all();
//        $consulta= Delegacion::find()
//                ->where(["poblacion"=>"santander"])
//                ->all();
//        $resultado= \yii\helpers\ArrayHelper::map($consulta,"id","id");
        
        
//        $lista1= Trabajadores::find()
//                ->where(["in","delegacion",$resultado])
//                ->asArray()
//                ->all();
        
        $lista1= Trabajadores::find()// esta es la solucion optima
                ->joinWith('delegacion0 d')
                ->where([
                    "d.poblacion"=>"Santander"
                ])
                ->all();
        
        $lista2= Trabajadores::find()                
                
                ->joinWith('delegacion0')                
                ->andWhere([
                    "<>","foto","null"
                ])
                ->asArray()
                ->all();
        
        $lista3= Delegacion::find()
                ->joinWith('trabajadores t')
                ->where([
                    "t.delegacion"=>null
                ])
                ->all();
        return $this->render('consulta20',[
            "lista"=>$lista,
            "lista1"=>$lista1,
            "lista2"=>$lista2,
            "lista3"=>$lista3,
        ]);
    }
    
    public function actionConsulta22(){
        $nombre= Trabajadores::getConsulta1();
        $datos= Trabajadores::getConsulta2();
        $listado= Trabajadores::getConsulta3();
                
        return $this->render('consulta22',[
            "nombre"=>$nombre,
            "datos"=>$datos,
            "listado"=>$listado,
        ]);
    }
}
